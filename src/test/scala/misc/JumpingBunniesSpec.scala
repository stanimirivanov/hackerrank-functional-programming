package misc

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class JumpingBunniesSpec extends FlatSpec {

  "Jumping bunnies" should "meet in point" in {
    val inputs = List(List(BigInt(2), BigInt(3), BigInt(4)), List(BigInt(1), BigInt(3)))
    val expected = List(BigInt(12), BigInt(3))

    val result = inputs.zip(expected).forall(t => JumpingBunnies.meetingPoint(t._1) == t._2)
    assert(result === true)
  }

}