package misc

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CommonDivisorsSpec extends PropSpec {

  val positiveInt = Gen.choose(1L, 100000000L)

  property("Common divisors value spec.") {
    val valueProp = Prop.forAll(positiveInt, positiveInt) { (n: Long, l: Long) =>
      val result = CommonDivisors.commonDivisors(n, l)
      result.forall {
        d => n % d == 0 && l % d == 0
      }
    }
    valueProp.check
  }

}
