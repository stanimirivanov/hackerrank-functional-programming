package misc

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class RotateStringSpec extends FlatSpec {

  "Rotate dtring" should "pass sample tests" in {
    val inputs = List[String]("abc", "abcde", "abab", "aaa", "z")
    val expected = List[IndexedSeq[String]](
      IndexedSeq("bca", "cab", "abc"),
      IndexedSeq("bcdea", "cdeab", "deabc", "eabcd", "abcde"),
      IndexedSeq("baba", "abab", "baba", "abab"),
      IndexedSeq("aaa", "aaa", "aaa"),
      IndexedSeq("z")
    )

    val result = inputs.zip(expected).forall(t => RotateString.rotations(t._1) == t._2)
    assert(result === true)
  }

}