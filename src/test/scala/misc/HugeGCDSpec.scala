package misc

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class HugeGCDSpec extends PropSpec {

  val positiveIntList = for {
    size <- Gen.choose(1, 1000)
    l <- Gen.listOfN(size, Gen.choose(1, 1000))
  } yield l

  property("Huge GCD value spec.") {
    val valueProp = Prop.forAll(positiveIntList, positiveIntList) { (a: List[Int], b: List[Int]) =>
      val n = a.map(BigInt(_)).product
      val m = b.map(BigInt(_)).product
      n.gcd(m) == HugeGCD.gcd(a, b)
    }
    valueProp.check
  }

}
