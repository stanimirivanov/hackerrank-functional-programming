package misc

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class RemoveDuplicatesSpec extends PropSpec {

  property("Remove duplicates size spec.") {
    val sizeProp = Prop.forAll { arr: List[Char] =>
      val str = arr.mkString
      RemoveDuplicates.removeDuplicates(str).length <= arr.size
    }
    sizeProp.check
  }

  property("Remove duplicates subset spec.") {
    val subsetProp = Prop.forAll { arr: List[Char] =>
      val str = arr.mkString
      val duplicatesRemoved = RemoveDuplicates.removeDuplicates(str)
      duplicatesRemoved.forall(c => arr.contains(c))
    }
    subsetProp.check
  }

  property("Remove duplicates uniqueness spec.") {
    val uniquenessProp = Prop.forAll { arr: List[Char] =>
      val str = arr.mkString
      val duplicatesRemoved = RemoveDuplicates.removeDuplicates(str).toList
      arr.forall(c => duplicatesRemoved.filter(_ == c).size == 1)
    }
    uniquenessProp.check
  }

  property("Remove duplicates index spec.") {
    val indexProp = Prop.forAll { arr: List[Char] =>
      val input = arr.toStream
      val str = arr.mkString
      val duplicatesRemoved = RemoveDuplicates.removeDuplicates(str).toStream
      duplicatesRemoved.forall(n => {val i = input.indexOf(n); i == -1 || duplicatesRemoved.indexOf(n) <= i })
    }
    indexProp.check
  }

}
