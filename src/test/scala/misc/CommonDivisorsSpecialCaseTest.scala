package misc

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CommonDivisorsSpecialCaseTest extends FlatSpec {

  "Common divisors" should "pass test case 10" in {
    val inputs = List[(Long, Long)]((9699690, 510510),
                        (9699690, 9699690),
                        (5336100, 9261000),
                        (100000000, 100000000),
                        (40465425, 26976950),
                        (67442375, 80930850),
                        (87318000, 43659000),
                        (62370000, 44550000),
                        (1299827, 1299821),
                        (93250113, 93250113))
    val expected = List[Long](128, 256, 81, 81, 72, 72, 480, 250, 1, 20)

    val result = inputs.zip(expected).forall { t =>
      val divisors = CommonDivisors.commonDivisors(t._1._1, t._1._2)
      divisors.size == t._2
    }
    assert(result === true)
  }

}