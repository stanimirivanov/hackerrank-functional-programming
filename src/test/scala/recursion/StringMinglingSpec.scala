package recursion

import org.junit.runner.RunWith
import org.scalacheck.{Prop, Gen}
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StringMinglingSpec extends PropSpec {

  val pq = for {
    size <- Gen.choose(1, 1000)
    s1 <- Gen.listOfN(size, Gen.alphaChar)
    s2 <- Gen.listOfN(size, Gen.alphaChar)
  } yield (s1, s2)

  property("String mingling size spec.") {
    val sizeProp = Prop.forAll(pq) { case (s1, s2) =>
      StringMingling.mingle(s1, s2).size == s1.size + s2.size
    }
    sizeProp.check
  }

  property("String mingling inclusion spec.") {
    val inclusionProp = Prop.forAll(pq) { case (s1, s2) =>
      val mingled = StringMingling.mingle(s1, s2)
      mingled.forall(c => s1.contains(c) || s2.contains(c))
    }
    inclusionProp.check
  }

  property("String mingling value spec.") {
    val valueProp = Prop.forAll(pq) { case (s1, s2) =>
      val mingled = StringMingling.mingle(s1, s2)
      val s1Array = s1.toArray
      val s2Array = s2.toArray
      mingled.grouped(2).zipWithIndex.forall {
        case (List(c1, c2), i) => c1 == s1Array(i) && c2 == s2Array(i)
      }
    }
    valueProp.check
  }

}