package recursion

import org.junit.runner.RunWith
import org.scalacheck.{Gen, Prop}
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FilterElementsSpec extends PropSpec {

  val smallInteger = Gen.choose(1, 1000)
  val elements = Gen.containerOf[List, String](Gen.alphaStr)

  property("Filter elements size spec.") {
    val sizeProp = Prop.forAll(elements, smallInteger) { (seq: List[String], k: Int) =>
      FilterElements.filter(seq, k).size <= seq.size
    }
    sizeProp.check
  }

  property("Filter elements subset spec.") {
    val sizeProp = Prop.forAll(elements, smallInteger) { (seq: List[String], k: Int) =>
      val filtered = FilterElements.filter(seq, k)
      filtered.forall(s => seq.contains(s))
    }
    sizeProp.check
  }

  property("Filter elements inclusion spec.") {
    val inclusionProp = Prop.forAll(elements, smallInteger) { (seq: List[String], k: Int) =>
      val filtered = FilterElements.filter(seq, k)
      filtered.forall(s => seq.filter(_ == s).toSet.size >= k)
    }
    inclusionProp.check
  }

  property("Filter elements exclusion spec.") {
    val exclusionProp = Prop.forAll(elements, smallInteger) { (seq: List[String], k: Int) =>
      val filtered = FilterElements.filter(seq, k)
      seq.forall(s => if (filtered.indexOf(s) == -1) seq.filter(_ == s).toSet.size < k else false)
    }
    exclusionProp.check
  }

  property("Filter elements index spec.") {
    val indexProp = Prop.forAll(elements, smallInteger) { (seq: List[String], k: Int) =>
      val input = seq.toStream
      val filtered = FilterElements.filter(seq, k).toStream
      filtered.forall(n => {val i = input.indexOf(n); i == -1 || filtered.indexOf(n) <= i })
    }
    indexProp.check
  }

}