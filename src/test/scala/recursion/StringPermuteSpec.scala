package recursion

import org.junit.runner.RunWith
import org.scalacheck.{Gen, Prop}
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StringPermuteSpec extends PropSpec {

  val evenSizedList = for {
    size <- Gen.choose(2, 200) suchThat (_ % 2 == 0)
    list <- Gen.listOfN(size, Gen.alphaChar)
  } yield list.mkString

  property("String-o-permute size spec.") {
    val sizeProp = Prop.forAll(evenSizedList) { s: String =>
      StringPermute.swap(s).size == s.size
    }
    sizeProp.check
  }

  property("String-o-permute inclusion spec.") {
    val inclusionProp = Prop.forAll(evenSizedList) { s: String =>
      val swapped = StringPermute.swap(s)
      swapped.toList.forall(c => s.contains(c))
    }
    inclusionProp.check
  }

  property("String-o-permute value spec.") {
    val valueProp = Prop.forAll(evenSizedList) { s: String =>
      val swapped = StringPermute.swap(s)
      s == StringPermute.swap(swapped)
    }
    valueProp.check
  }

}