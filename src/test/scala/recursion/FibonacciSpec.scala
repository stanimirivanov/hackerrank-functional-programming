package recursion

import org.junit.runner.RunWith
import org.scalacheck.{Gen, Prop}
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FibonacciSpec extends PropSpec {

  property("Fibonacci value spec.") {

    val smallInteger = Gen.choose(0,1000)

    val valueProp = Prop.forAll(smallInteger) { n =>
      val result = Fibonacci.fibonacci(n)
      if (n == 0 || n == 1) result == 0
      else result == Fibonacci.fibonacci(n - 1) + Fibonacci.fibonacci(n - 2)
    }
    valueProp.check
  }

}