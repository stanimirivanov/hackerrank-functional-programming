package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PrefixCompressionSpec extends FlatSpec {

  "Prefix compression" should "pass sample cases" in {
    val inputs: List[(String, String)] = List(
      ("abcdefpr", "abcpqr"),
      ("kitkat", "kit"),
      ("puppy", "puppy")

    )
    val expected: List[(String, String, String)] = List(
      ("3 abc", "5 defpr", "3 pqr"),
      ("3 kit", "3 kat", "0"),
      ("5 puppy", "0", "0")
    )

    val result = inputs.zip(expected).forall(t => PrefixCompression.prefixCompression(t._1._1, t._1._2) == t._2)
    assert(result === true)
  }

}