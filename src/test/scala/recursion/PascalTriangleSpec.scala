package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PascalTriangleSpec extends FlatSpec {

  "Jumping bunnies" should "meet in point" in {
    val inputs = List(1, 2, 3, 4, 5)
    val expected = List(List(1), List(1, 1), List(1, 2, 1), List(1, 3, 3, 1), List(1, 4, 6, 4, 1))

    val result = inputs.zip(expected).forall(t => PascalTriangle.triangle(t._1) == t._2)
    assert(result === true)
  }

}