package recursion

import org.junit.runner.RunWith
import org.scalacheck.Prop
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class EuclideanGCDSpec extends PropSpec {

  // Note: gcd is unique up to multiplication by a unit, i.e. 1 or -1
  property("GCD value spec.") {
    val valueProp = Prop.forAll { (x: Int, y: Int) =>
      val expected = BigInt(x).gcd(BigInt(y))
      val result = EuclideanGCD.gcd(x, y)
      math.abs(expected.toInt) == math.abs(result)
    }
    valueProp.check
  }

}