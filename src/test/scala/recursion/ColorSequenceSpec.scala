package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ColorSequenceSpec extends FlatSpec {

  "Color sequence" should "pass sample cases" in {
    val inputs: List[String] = List("RGGR", "RYBG", "RYRB", "YGYGRBRB")
    val expected: List[Boolean] = List(true, true, false, false)

    val result = inputs.zip(expected).forall(t => ColorSequence.isSequenceFullOfColors(t._1).valid == t._2)
    assert(result === true)
  }

}