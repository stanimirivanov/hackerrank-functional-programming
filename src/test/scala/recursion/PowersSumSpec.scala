package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PowersSumSpec extends FlatSpec {

  "Sums of Powers" should "pass sample tests" in {
    val inputs = List[(Int, Int)]((10, 2), (100, 3), (100, 2))
    val expected = List[Int](1, 1, 3)

    val result = inputs.zip(expected).forall(t => PowersSum.numberOfWays(t._1._1, t._1._2) == t._2)
    assert(result === true)
  }

}