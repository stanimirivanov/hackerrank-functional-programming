package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SuperDigitSpec extends FlatSpec {

  "Super Digit" should "pass sample cases" in {
    val inputs: List[BigInt] = List(148 * 3)
    val expected: List[Int] = List(3)

    val result = inputs.zip(expected) forall { t =>
      SuperDigit.superDigit(t._1) == t._2
    }
    assert(result === true)
  }

}