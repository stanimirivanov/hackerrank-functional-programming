package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StringReductionsSpec extends FlatSpec {

  "String Reductions" should "pass sample cases" in {
    val inputs: List[String] = List("accabb", "abc", "pprrqq")
    val expected: List[String] = List("acb", "abc", "prq")

    val result = inputs.zip(expected) forall { t =>
      StringReductions.reduce(t._1) == t._2
    }
    assert(result === true)
  }

}