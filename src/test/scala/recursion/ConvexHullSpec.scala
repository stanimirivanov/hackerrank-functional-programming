package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.io.Source
import scala.util.{Failure, Success, Try}

@RunWith(classOf[JUnitRunner])
class ConvexHullSpec extends FlatSpec {

  "Convex hull" should "pass sample cases" in {
    import ConvexHull.Point

    val inputs: List[List[Point]] = List(
      List(Point(1, 1), Point(2, 5), Point(3, 3), Point(5, 3), Point(3, 2), Point(2, 2)),
      List(Point(3, 2), Point(2, 5), Point(4, 5)))

    val expected: List[Double] = List(12.2, 8.3)

    val result = inputs.zip(expected).forall { t =>
      val hull = ConvexHull.convexHull(t._1)
      val perim = ConvexHull.perimeter(hull)
      math.abs(perim - t._2) < 0.1
    }
    assert(result === true)
  }

  "Convex hull" should "pass test case 4" in {
    import ConvexHull.Point

    val PATH = getClass.getResource("").getPath
    val fileName = PATH + "convexhull-testcase4.txt"

    def readTextFile(fileName: String): Try[Iterator[String]] = {
      Try(Source.fromFile(fileName).getLines())
    }

    readTextFile(fileName) match {
      case Success(lines) => {
        val points = lines.map(s => { val a = s.split(" "); Point(a(0).toDouble, a(1).toDouble) }).toList
        val hull = ConvexHull.convexHull(points)
        val perim = ConvexHull.perimeter(hull)

        val expected = 3874.1
        this.assert(true === (math.abs(perim - expected) < 0.1))
      }
      case Failure(f) => {
        println(f)
        fail(f)
      }
    }
  }

}