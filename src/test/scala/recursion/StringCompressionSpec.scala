package recursion

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StringCompressionSpec extends FlatSpec {

  "String compression" should "pass sample cases" in {
    val inputs: List[String] = List("abcaaabbb", "abcd", "aaabaaaaccaaaaba")
    val expected: List[String] = List("abca3b3", "abcd", "a3ba4c2a4ba")

    val result = inputs.zip(expected).forall(t => StringCompression.compress(t._1) == t._2)
    assert(result === true)
  }

}