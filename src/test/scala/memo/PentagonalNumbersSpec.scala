package memo

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PentagonalNumbersSpec extends FlatSpec {

  "Pentagonal Numbers" should "pass sample cases" in {
    val inputs: List[Long] = List(1, 2, 3, 4, 5)
    val expected: List[BigInt] = List(1, 5, 12, 22, 35)

    val result = inputs.zip(expected) forall { t =>
      PentagonalNumbers.pentagonal(t._1) == t._2
    }
    assert(result === true)
  }

}
