package memo

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FibonacciSpec extends PropSpec {

  property("Fibonacci value spec.") {
    val nonNegativeInt = Gen.choose(0, Int.MaxValue)

    val valueProp = Prop.forAll(nonNegativeInt) { n: Int =>
      val result = Fibonacci.fibonacci(n)
      if (n == 0) result == 0
      else if (n == 1) result == 1
      else result == Fibonacci.fibonacci(n - 1) + Fibonacci.fibonacci(n - 2)
    }
    valueProp.check
  }

}