package memo

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BinarySearchTreeCountSpec extends PropSpec {
  
  BinarySearchTreeCount.init
  
  property("Number of Binary Search Tree value spec.") {
    val positiveInt = Gen.choose(1, 1000)

    val valueProp = Prop.forAll(positiveInt) { n: Int =>
      BinarySearchTreeCount.countBST(n) == Cn(n)
    }
    valueProp.check
  }

  import spire.implicits._
  import spire.math._

  def Cn(n: Int): BigInt =
    (BigInt(2) to BigInt(n)).map(k => Rational(n + k, k)).qproduct.toBigInt

}