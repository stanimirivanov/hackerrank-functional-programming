package functionalstructures

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ValidBSTSpec extends FlatSpec {

  "Valid BST" should "pass test 0" in {
    val inputs = List[List[Int]](List(1,2,3), List(2,1,3), List(3,2,1,5,4,6), List(1,3,4,2), List(3,4,5,1,2))
    val expected = List[Boolean](true, true, true, false, false)

    val result = inputs.zip(expected).forall(t => ValidBST.validPreorderTraversal(t._1) == t._2)
    assert(result === true)
  }

  "Valid BST" should "pass test 1" in {
    val inputs = List[List[Int]](List(3,2,1), List(1), List(2,3,1), List(2,1,3), List(3,1,4,2), List(2,3,6,7,4,8,9,5,1), List(3,2,1,4,6,5), List(3,2,1,4,5,6), List(5,1,4,2,6,3))
    val expected = List[Boolean](true, true, false, true, false, false, true, true, false, true)

    val result = inputs.zip(expected).forall(t => ValidBST.validPreorderTraversal(t._1) == t._2)
    assert(result === true)
  }

}