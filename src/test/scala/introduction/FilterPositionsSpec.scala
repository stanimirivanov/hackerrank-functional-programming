package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FilterPositionsSpec extends PropSpec {

  property("Filter positions size spec.") {
    val sizeProp = Prop.forAll { arr: List[Int] =>
      if (arr.isEmpty) {
        FilterPositions.f(arr).size == 0
      } else if (arr.size % 2 == 0) {
        FilterPositions.f(arr).size == arr.size / 2
      } else {
        FilterPositions.f(arr).size == (arr.size - 1) / 2
      }
    }
    sizeProp.check
  }

  property("Filter positions subset spec.") {
    val subsetProp = Prop.forAll { arr: List[Int] =>
      val filtered = FilterPositions.f(arr)
      filtered.forall(n => arr.contains(n))
    }
    subsetProp.check
  }

}
