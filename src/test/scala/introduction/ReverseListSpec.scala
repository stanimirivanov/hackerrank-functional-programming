package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ReverseListSpec extends PropSpec {

  property("Reverse list reverse spec.") {
    val reverseProp = Prop.forAll { arr: List[Int] =>
      ReverseList.f(arr) == arr.reverse
    }
    reverseProp.check
  }
}
