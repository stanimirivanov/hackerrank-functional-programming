package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ListReplicationSpec extends PropSpec {

  val positiveInt = Gen.choose(1, 100)
  val list = Gen.listOf(Gen.choose(Int.MinValue, Int.MaxValue))

  property("List replication size spec.") {
    val sizeProp = Prop.forAll(positiveInt, list) { (num: Int, arr: List[Int]) =>
      ListReplication.f(num, arr).size == (num * arr.size)
    }
    sizeProp.check
  }

  property("List replication equality spec.") {
    val equalityProp = Prop.forAll(positiveInt, list) { (num: Int, arr: List[Int]) =>
      ListReplication.f(num, arr).toSet.equals(arr.toSet)
    }
    equalityProp.check
  }

  property("List replication grouping spec.") {
    val groupingProp = Prop.forAll(positiveInt, list) { (num: Int, arr: List[Int]) =>
      if  (arr.isEmpty) {
        ListReplication.f(num, arr).isEmpty
      } else {
        val groups = ListReplication.f(num, arr).grouped(num)
        groups.foldLeft(true)((acc, xss) => acc && xss.size == num && xss.toSet.size == 1)
      }
    }
    groupingProp.check
  }
}
