package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FilterArraySpec extends PropSpec {

  property("Filter array size spec.") {
    val sizeProp = Prop.forAll { (delim: Int, arr: List[Int]) =>
      FilterArray.f(delim, arr).size <= arr.size
    }
    sizeProp.check
  }

  property("Filter array subset spec.") {
    val subsetProp = Prop.forAll { (delim: Int, arr: List[Int]) =>
      val filtered = FilterArray.f(delim, arr)
      filtered.foldLeft(true)((acc, n) => arr.contains(n))
    }
    subsetProp.check
  }

  property("Filter array exclusion spec.") {
    val exclusionProp = Prop.forAll { (delim: Int, arr: List[Int]) =>
      ! FilterArray.f(delim, arr).contains(delim)
    }
    exclusionProp.check
  }

  property("Filter array index spec.") {
    val indexProp = Prop.forAll { (delim: Int, arr: List[Int]) =>
      val input = arr.toStream
      val filtered = FilterArray.f(delim, arr).toStream
      filtered.forall(n => {val i = input.indexOf(n); i == -1 || filtered.indexOf(n) <= i })
    }
    indexProp.check
  }

}
