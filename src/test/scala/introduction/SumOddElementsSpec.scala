package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SumOddElementsSpec extends PropSpec {

  val nonNegativeIntList = Gen.listOf(Gen.choose(0, 1000))

  val negativeIntList = Gen.listOf(Gen.choose(-1000, -1))

  property("Sum of non negative odd elements inequality spec.") {
    val inequalityProp = Prop.forAll(nonNegativeIntList) { arr: List[Int] =>
      SumOddElements.f(arr) <= arr.sum
    }
    inequalityProp.check
  }

  property("Sum of negative odd elements inequality spec.") {
    val inequalityProp = Prop.forAll(negativeIntList) { arr: List[Int] =>
      SumOddElements.f(arr) >= arr.sum
    }
    inequalityProp.check
  }
}
