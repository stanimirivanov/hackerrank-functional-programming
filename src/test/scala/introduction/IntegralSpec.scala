package introduction

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class IntegralSpec extends FlatSpec {

  "An Integral" should "calculate area and volume" in {
    val coefficients:List[Int] = List(1, 2, 3, 4, 5)
    val powers:List[Int] = List(6, 7, 8, 9, 10)
    val limits:List[Int] = List(1, 4)

    val area = Integral.summation(Integral.f, limits.reverse.head, limits.head, coefficients, powers)
    val volume = Integral.summation(Integral.area, limits.reverse.head, limits.head, coefficients, powers)

    println(area)
    println(volume)
    assert(area === 2435300.3)
    assert(volume === 2.61729511689407E13)
  }

}