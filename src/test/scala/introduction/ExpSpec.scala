package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExpSpec extends PropSpec {

  val smallDouble = Gen.choose(-10.0, 10.0)

  property("Exp value spec.") {
    val valueProp = Prop.forAll(smallDouble) { x: Double =>
      val expected = math.exp(x)
      val result = Exp.f(x)
      expected - 0.0001 <= math.abs(result) || math.abs(result) <= expected + 0.0001
    }
    valueProp.check
  }

}