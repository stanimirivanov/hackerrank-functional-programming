package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NElementsArraySpec extends PropSpec {

  val positiveInt = Gen.choose(1, 100)

  property("Array Of N Elements size spec.") {
    val sizeProp = Prop.forAll(positiveInt) { num: Int =>
      NElementsArray.f(num).size == num
    }
    sizeProp.check
  }

  property("Array Of N Elements elements spec.") {
    val elementsProp = Prop.forAll(positiveInt) { num: Int =>
      val indexed = NElementsArray.f(num).zipWithIndex
      indexed.forall(t => correctElementAtIndex(t._1, t._2))
    }
    elementsProp.check
  }

  private def correctElementAtIndex(elem: Int, index: Int) =
    elem == index + 1 || elem == 1
}
