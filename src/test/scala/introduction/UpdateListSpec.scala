package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class UpdateListSpec extends PropSpec {

  property("Update list size spec.") {
    val sizeProp = Prop.forAll { arr: List[Int] =>
      UpdateList.f(arr).size == arr.size
    }
    sizeProp.check
  }

  property("Update list content spec.") {
    val sizeProp = Prop.forAll { arr: List[Int] =>
      val zipped = arr.zip(UpdateList.f(arr))
      zipped.forall(t => correctContentAtPosition(t._1, t._2))
    }
    sizeProp.check
  }

  private def correctContentAtPosition(input: Int, result: Int): Boolean =
    result == math.abs(input)
}
