package introduction

import org.junit.runner.RunWith
import org.scalacheck._
import org.scalatest.PropSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ListLengthSpec extends PropSpec {

  property("List length size spec.") {
    val sizeProp = Prop.forAll { arr: List[Int] =>
      ListLength.f(arr) == arr.size
    }
    sizeProp.check
  }

}
