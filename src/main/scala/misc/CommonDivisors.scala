package misc

/**
 * Mario and Luigi are partners at a very famous game store.
 * They are also expert in a game where they have to save the princess from
 * the clutches of a dragon. While achieving this objective, they passes
 * through many difficulties, sometime easy sometime hard. For clearing
 * each difficulty they get some points.
 * A player's point after the game is the total of all these points.
 *
 * Let's denote Mario's points by M and that of Luigi by L.
 * Now Princess Peach is wondering how many positive
 * integers are there that are divisor to both number, M and L.
 * Help her in the same.
 *
 * @see http://www.hackerrank.com/challenges/common-divisors
 */
object CommonDivisors {

  def commonDivisors(n: Long, l: Long): Set[Long] = {
    val divisorsN = divisors(n)
    val divisorsL = divisors(l)
    divisorsN intersect divisorsL
  }

  def divisors(n: Long): Set[Long] = {
    val nFactors = factors(n)
    val divisorSet = (2 to nFactors.size).flatMap(i => nFactors.combinations(i)).toSet
    val all = divisorSet.map(l => l.product) union nFactors.toSet union Set(1)
    all
  }

  def factors(n: Long): List[Long] = {
    val exists = (2L to math.sqrt(n.toDouble).toLong).find(n % _ == 0)
    exists match {
      case Some(d) => d :: factors(n / d)
      case None => List(n)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach { line =>
      val list = line.split(" ").toList
      val n = list.head.toLong
      val l = list.tail.head.toLong
      val result = commonDivisors(n, l)
      println(result.size)
    }
  }

}
