package misc

/**
 * You are given a string, str, of length N consisting of lowercase letters of alphabet.
 * You have to remove all those characters from str which have already appeared in it,
 * i.e., you have to keep only first occurrence of each letter.
 *
 * @see http://www.hackerrank.com/challenges/remove-duplicates
 */
object RemoveDuplicates {

  def removeDuplicates(str: String): String =
    str.toList.distinct.mkString

  def main(args: Array[String]) {
    val str = io.Source.stdin.getLines().toList.head
    val result = removeDuplicates(str)
    println(result)
  }

}
