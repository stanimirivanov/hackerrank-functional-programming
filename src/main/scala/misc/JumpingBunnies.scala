package misc

/**
 * Bunnies are very cute animals who likes to jump a lot. Every bunny has his own range of jump.
 * Lets say there are N bunnies and ith (i in [1, N]) bunny jumps ji units. Consider a 1-D plane, where
 * initially bunnies are at 0. All of them starts jumping in forward direction.
 *
 * For example, consider the case of k-th bunny. Initially he is at 0. After first jump, he will be at point jk.
 * After second, he will be at _2*jk_ and so on. After mth jump, he will be at point _m*jk_.
 *
 * Two bunnies can meet each other when they are only at ground. When at ground, a bunny can wait any amount of time.
 * Being a social animal, all of them decide to meet at the next point where all of them will be at ground.
 * So you have to tell them the nearest point where all bunnies can meet.
 *
 * For example, if there are N = 3 bunnies where j1 = 2, j2 = 3, j3 = 4.
 * Nearest point where all bunnies can meet again is at 12. First bunny has to jump six times,
 * for second it is 4 times and for third it is 3 times.
 *
 * Help bunnies to find the nearest point where they can meet again.
 *
 * @see http://www.hackerrank.com/challenges/jumping-bunnies
 */
object JumpingBunnies {

  def gcd(x: BigInt, y: BigInt): BigInt = if (y == 0) x else gcd(y, x % y)

  def lcm(x: BigInt, y: BigInt): BigInt = x * (y / gcd(x, y))

  def meetingPoint(points: List[BigInt]): BigInt = {
    points.foldLeft(BigInt(1))((acc, p) => lcm(acc, p))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail.head.split(" ").toList.map((x) => BigInt(x)))
  }

  def printResult(points: List[BigInt]): Unit = {
    val result = meetingPoint(points)
    println(result)
  }

}
