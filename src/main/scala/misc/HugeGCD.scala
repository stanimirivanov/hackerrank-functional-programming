package misc

/**
 * Gayasen has received a homework assignment to compute the greatest common divisor of the
 * two positive integers A and B. Since the numbers are quite large, the professor provided
 * him with N smaller integers whose product is A, and M integers with product B. He would
 * like to verify result, so he has asked you to write a program to solve his problem.
 * But instead of printing complete answer you have to print answer modulo 109+7.
 *
 * @see http://www.hackerrank.com/challenges/huge-gcd-fp
 */
object HugeGCD {

  def gcd(a: List[Int], b: List[Int]): BigInt = {
    val n = a.map(BigInt(_)).product
    val m = b.map(BigInt(_)).product
    n.gcd(m)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    val a = lines(1).split(" ").toList.map(_.toInt)
    val b = lines(3).split(" ").toList.map(_.toInt)
    val result = gcd(a, b)
    println(result % BigInt(1000000007))
  }

}
