package misc

/**
 * Scturtle likes strings very much. He is getting bored today, because he has already completed
 * this week's task and doesn't have anything else to do. So he starts left-rotating a string.
 * If the length of the string is n, then he will rotate it n times and note down the result of
 * each rotation on a paper.
 *
 * For a string S=s1s2…sn, n rotations are possible.
 * Let's represent these rotations by r1,r2…rn.
 * Rotating it once will result in string r1=s2s3…sns1, rotating it again will result in
 * string r2=s3s4…sns1s2 and so on. Formally, ith rotation will be equal to ri=si+1…sn−1sns1…si.
 * Note that rn=S.
 *
 * Your task is to display all n rotations of string S.
 *
 * For example, if S = abc then it has 3 rotations. They are  r1 = bca, r2 = cab and r3 = abc.
 *
 * @see http://www.hackerrank.com/challenges/rotate-string
 */
object RotateString {

  def rotations(s: String): IndexedSeq[String] = {
    val ls = s.toList
    val range = (1 until ls.size) union (0 to 0)
    range.map(n => ((ls drop n) ::: (ls take n)).mkString)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val result = rotations(s)
    println(result.mkString(" "))
  }

}
