package functionalstructures

/**
 * A binary tree is a tree where each node has at most two children. It is characterized by
 * any of the following properties:
 *
 * 1. It can be an empty tree, where root = null.
 * 2. It can contain a root node which contain some value and two subtree, left subtree and
 *    right subtree, which are also binary tree.
 *
 * A binary tree is a binary search tree (BST) if all the non-empty nodes follows both two properties:
 *
 * 1. Each node's left subtree contains only values less than it, and
 * 2. Each node's right subtree contains only values greater than it.
 *
 * Preorder traversal is a tree traversal method where the current node is visited first, then the
 * left subtree and then the right subtree. More specifically, let's represent the preorder traversal
 * of a tree by a list. Then this list is constructed in following way:
 *
 * If the tree is empty, then this list be a null list.
 * For non-empty tree, let's represent the preorder of left subtree as L and of right subtree as R.
 * Then the preorder of tree is obtained by appending L to current node, and then appending R to it.
 *
 * 1         2          3
 *  \       / \        / \
 *   3     1   3      2   5
 *  /                /   / \
 * 2                1   4   6
 *
 * (a) (b) (c)
 *
 * For the above trees, preorder will be
 *
 * (a) 1 3 2
 * (b) 2 1 3
 * (c) 3 2 1 5 4 6
 *
 * Given a list of numbers, determine whether it can represent the preorder traversal of a binary
 * search tree(BST).
 *
 * @see https://www.hackerrank.com/challenges/valid-bst
 */

abstract sealed class Tree {
  def value: Int
  def left: Tree
  def right: Tree
  def isEmpty: Boolean

  def add(x: Int): Tree =
    if (isEmpty) Branch(x)
    else if (x < value) Branch(value, left.add(x), right)
    else if (x > value) Branch(value, left, right.add(x))
    else this

  def valuesByDepth: List[Int] = {
    def loop(s: List[Tree]): List[Int] =
      if (s.isEmpty) Nil
      else if (s.head.isEmpty) loop(s.tail)
      else s.head.value :: loop(s.head.left :: s.head.right :: s.tail)

    loop(List(this))
  }

  def fail(m: String) = throw new NoSuchElementException(m)

  override def toString: String =
    if (isEmpty) "."
    else "{" + left + value + right + "}"

}

case object Leaf extends Tree {
  def value: Int = fail("Empty tree.")
  def left: Tree = fail("Empty tree.")
  def right: Tree = fail("Empty tree.")
  def isEmpty: Boolean = true
}

case class Branch(value: Int, left: Tree = Leaf, right: Tree = Leaf) extends Tree {
  def isEmpty: Boolean = false
}

object Tree {
  def empty: Tree = Leaf

  def apply(xs: List[Int]): Tree = {
    var r: Tree = Tree.empty
    for (x <- xs) r = r.add(x)
    r
  }
}

object ValidBST {

  def validPreorderTraversal(xs: List[Int]): Boolean = {
    val tree = Tree(xs)
    tree.valuesByDepth == xs
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(ls: List[String]): Unit = {
    val xs = ls.tail.head.split(" ").map(_.toInt).toList
    val result = validPreorderTraversal(xs)
    if (result) println("YES") else println("NO")
  }

}
