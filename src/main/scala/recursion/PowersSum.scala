package recursion

/**
 * Find the number of ways in which a given integer, X, can be expressed as
 * the sum of the N-th power of unique natural numbers.
 *
 * @see http://www.hackerrank.com/challenges/functional-programming-the-sums-of-powers
 */
object PowersSum {

  def numberOfWays(x: Int, power: Int) : Int = {
    def loop(idx: Int, sum: Int): Int = {
      val nextSum = sum + math.pow(idx, power).toInt
      if (nextSum > x) 0
      else if (nextSum == x) 1
      else loop(idx + 1, nextSum) + loop(idx + 1, sum)
    }
    loop(1, 0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val x = lines(0).toInt
    val power = lines(1).toInt
    val result = numberOfWays(x, power)
    println(result)
  }

}
