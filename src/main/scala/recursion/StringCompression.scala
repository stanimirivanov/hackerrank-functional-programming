package recursion

/**
 * Joseph and Javran are making a contest for the apes. During the process they have
 * to communicate frequently with each other. Since they are not fully evolved as
 * humans, they cannot speak properly. So they have to transfer messages using
 * postcards of small sizes.
 *
 * To save space on the small postcards, they devise a not-so-uncommon string compression algorithm:
 *
 * If a character, ch, occurs n(>1) times in a row, then it will be represented
 * by {ch}{n}, where {x} is the value of x. For example, if the substring is a
 * sequence of 4 'a' ("aaaa"), it will be represented as "a4".
 *
 * If a character, ch, occurs exactly one time in a row, then it will be simply represented
 * as {ch}. For example, if the substring is "a", then it will be represented as "a".
 *
 * Help Joseph to compress a message, msg, which will be sent to Javran.
 *
 * @see http://www.hackerrank.com/challenges/string-compression
 */
object StringCompression {

  def compress(msg: String): String =
    compressList(msg.toList, 1).mkString

  def compressList(seq: List[Char], key: Int): List[Char] = seq match {
    case x1 :: x2 :: xs if (x1 == x2) 				      => compressList(x2 :: xs, key + 1)
    case x1 :: x2 :: xs if (x1 != x2 && key == 1) 	=> x1 :: compressList(x2 :: xs, 1)
    case x1 :: x2 :: xs if (x1 != x2 && key > 1) 	  => x1 :: encode(key) ::: compressList(x2 :: xs, 1)
    case x :: _ if (key == 1) 						          => x :: compressList(Nil, 1)
    case x :: _ if (key > 1) 						            => x :: encode(key) ::: compressList(Nil, 1)
    case Nil 										                    => Nil
  }

  def encode(key: Int): List[Char] = key.toString.toList

  def main(args: Array[String]) {
    val line = io.Source.stdin.getLines().toList.head
    val result = compress(line)
    print(result)
  }

}
