package recursion

/**
 * Given a string, str= "s1s2 ... sn", consisting of n lower case latin characters
 * ('a'-'z'), remove all of the characters that occurred previously in the
 * string. That is, remove all characters, si, for which
 * [Exists] j, sj=si and j<i
 *
 * @see https://www.hackerrank.com/challenges/string-reductions
 */
object StringReductions {

  def reduce(s: String): String =
    s.toList.distinct.mkString

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(line: String): Unit = {
    val result = reduce(line)
    println(result)
  }

}
