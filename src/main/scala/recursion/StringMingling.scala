package recursion

/**
 * Pawel and Shaka recently became friends. On their planet, it is believed that their friendship
 * will last forever, if they merge their favorite strings and etch it on the surface of a stone.
 * So we will mingle their favorite strings. The lengths of their favorite strings is same (say n).
 * Mingling two strings, P=p1p2…pn and Q=q1q2…qn, both of length n, will result in creation of a
 * new string R of length 2×n. It will have the following structure:
 *
 * R=p1q1p2q2…pnqn
 *
 * You are given two strings P (favorite of Pawel) and Q (favorite of Shaka), find the mingled string R.
 *
 * @see http://www.hackerrank.com/challenges/string-mingling
 */
object StringMingling {

  def mingle(s1: List[Char], s2: List[Char]): List[Char] = s1 match {
    case Nil => Nil
    case x :: xs => s2 match {
      case y :: ys => x :: y :: mingle(xs, ys)
      case _ => Nil
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toArray
    val P: List[Char] = lines(0).toList
    val Q: List[Char] = lines(1).toList
    val result = mingle(P, Q)
    print(result.mkString)
  }

}
