package recursion

/**
 * You are given a sequence of N balls in 4 colors: red, green, yellow and blue.
 * The sequence is full of colors if and only if all of the following conditions are true:
 *
 * There are as many red balls as green balls.
 * There are as many yellow balls as blue balls.
 * Difference between the number of red balls and green balls in every prefix of the sequence is at most 1.
 * Difference between the number of yellow balls and blue balls in every prefix of the sequence is at most 1.
 *
 * Your task is to write a program, which for a given sequence
 * prints True if it is full of colors, otherwise it prints False.
 *
 * @see http://www.hackerrank.com/challenges/sequence-full-of-colors
 */
object ColorSequence {

  case class ColorsState(r: Int, g: Int, y: Int, b: Int) {

    def next(color: Char): ColorsState = color match {
      case 'R' => this.copy(r = r + 1)
      case 'G' => this.copy(g = g + 1)
      case 'Y' => this.copy(y = y + 1)
      case 'B' => this.copy(b = b + 1)
    }

    def validRuleOne(): Boolean = r == g
    def validRuleTwo(): Boolean = y == b
    def validRuleThree(): Boolean = math.abs(r - g) <= 1
    def validRuleFour(): Boolean = math.abs(y - b) <= 1

    def valid(): Boolean = validRuleOne && validRuleTwo && validRuleThree && validRuleFour

    override def toString() =
      if (valid) "True" else "False"
  }

  def isSequenceFullOfColors(line: String): ColorsState = {
    def iter(input: List[Char], state: ColorsState): ColorsState = input match {
      case Nil => state
      case x :: xs => {
        val nextState = state.next(x)
        if (nextState.validRuleThree && nextState.validRuleFour) iter(xs, nextState)
        else nextState
      }
    }
    iter(line.toList, ColorsState(0, 0, 0, 0))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(line: String): Unit = {
    println(isSequenceFullOfColors(line))
  }

}
