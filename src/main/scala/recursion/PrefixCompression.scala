package recursion

/**
 * You are incharge of data transfer between two data-centres. Each set of data is represented
 * by a pair of strings. Over a period of time you have observed a trend: most of the times
 * both strings share some prefix. You want to utilize this observation to design a data
 * compression algorithm which will be used to reduce amount of data to be transferred.
 *
 * You are given two strings, x and y, representing the data, you need to find the longest common
 * prefix (p) of the two strings. Then you will send substring p, x′ and y′, where x′ and y′ are the
 * substring left after stripping p from them.
 *
 * For example, if x=‘‘abcdefpr" and y=‘‘abcpqr", then p=‘‘abc",x′=‘‘defpr",y′=‘‘pqr".
 *
 * @see http://www.hackerrank.com/challenges/prefix-compression
 */
object PrefixCompression {

  def prefixCompression(x: String, y: String): (String, String, String) = {
    val xList = x.toList
    val yList = y.toList
    val p = prefix(x.toList, y.toList)
    (encode(p), encode(xList drop p.size), encode(yList drop p.size))
  }

  def prefix(x: List[Char], y: List[Char]): List[Char] = x match {
    case Nil => Nil
    case x :: xs => y match {
      case Nil => Nil
      case y :: ys => if (x == y) x :: prefix(xs, ys) else Nil
    }
  }

  def encode(s: List[Char]): String = s match {
    case Nil => "0"
    case _ => s.size + " " + s.mkString
  }

  def main(args: Array[String]) {
    val x = io.Source.stdin.getLines().toList.head.toList
    val y = io.Source.stdin.getLines().toList.head.toList
    val p = prefix(x, y)
    println(encode(p))
    println(encode(x drop p.size))
    println(encode(y drop p.size))
  }



}
