package recursion

/**
 * We define super digit of an integer x using the following rules:
 *
 * Iff x has only 1 digit, then its super digit is x. Otherwise, the super digit
 * of x is equal to the super digit of the digit-sum of x. Here, digit-sum of a
 * number is defined as the sum of its digits. For example, super digit of 9875
 * will be calculated as:
 *
 * super-digit(9875) = super-digit(9+8+7+5)
 *                   = super-digit(29)
 *                   = super-digit(2+9)
 *                   = super-digit(11)
 *                   = super-digit(1+1)
 *                   = super-digit(2)
 *                   = 2.
 *
 * You are given two numbers - n k. You have to calculate the super digit of P.
 *
 * P is created when number n is concatenated k times. That is, if n = 123 and k
 * = 3, then P = 123123123.
 *
 * @see https://www.hackerrank.com/challenges/super-digit
 */
object SuperDigit {

  @annotation.tailrec
  def superDigit(p: BigInt): Int = {
    def digitSum(num: BigInt): BigInt =
      num.toString.map(_.asDigit).sum

    if (p < 10) p.toInt
    else {
      val sum = digitSum(p)
      superDigit(sum)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines)
  }

  def printResult(lines: List[String]): Unit = {
    val Array(n, k) = lines.head.split(" ").map(BigInt(_))
    val result = superDigit(n * k)
    println(result)
  }

}
