package recursion

/**
 * The Fibonacci series begins with 0 and 1 (which are the first and the second terms respectively).
 * After this, every element is the sum of the preceding elements: i.e,
 *
 * Fibonacci(n) = Fibonacci(n-1) + Fibonacci(n-2)
 *
 * Given the starter code, complete the fibonacci function to return the Nth term.
 *
 * Please note, that we start counting from Fibonacci(1) = 0.
 * This might differ from some other notations which treat Fibonacci(0) = 0.
 *
 * @see http://www.hackerrank.com/challenges/functional-programming-warmups-in-recursion---fibonacci-numbers
 */
object Fibonacci {

  lazy val fib: Stream[BigInt] = 0 #:: 1 #:: fib.zip(fib.tail).map(p => p._1 + p._2)

  def fibonacci(n: Int): BigInt = {
    fib.take(n + 1).head
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    val n: Int = lines.head.toInt
    val result = fibonacci(n)
    println(result)
  }

}
