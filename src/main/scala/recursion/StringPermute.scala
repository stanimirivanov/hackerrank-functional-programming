package recursion

/**
 * One very good day, Kazama gave Shinchan a string of even length, and asked him to swap the
 * characters at even position with the next character. Indexing starts from 0.
 * Formally, given a string str of length L, where L is even, Shinchan has to swap the
 * characters at position i and i+1, where i in {0, 2,.., L-2}.
 *
 * For example, if str = "abcdpqrs", then we have to swap the characters at
 * position {(0, 1), (2, 3), (4, 5), (6, 7)} as L = 8. So answer will be "badcqpsr".
 *
 * @see http://www.hackerrank.com/challenges/string-o-permute
 */
object StringPermute {

  def swap(s: String): String = {
    val swapped = s.toList.grouped(2) flatMap { pair =>
      pair match {
        case List(even, odd) => List(odd, even)
        case _               => Nil
      }
    }
    swapped.mkString
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    val swaps = lines.tail map { swap(_) }
    val result = swaps mkString "\n"
    print(result)
  }

}
