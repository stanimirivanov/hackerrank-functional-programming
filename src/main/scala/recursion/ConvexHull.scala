package recursion

import scala.collection.mutable.ListBuffer

object ConvexHull {
  
  case class Point(x: Double, y: Double) extends Ordered[Point] {
    def distanceTo(that: Point): Double = {
      val dx = this.x - that.x
      val dy = this.y - that.y
      math.sqrt(dx * dx + dy * dy)
    }

    def compare(that: Point): Int = {
      if (this.y < that.y) return -1
      if (this.y > that.y) return +1
      if (this.x < that.x) return -1
      if (this.x > that.x) return +1
      return 0
    }
  }

  def convexHull(points: List[Point]): List[Point] = {
    val sorted = points.sorted
    val upper = halfHull(sorted).tail
    val lower = halfHull(sorted.reverse).tail
    upper ++ lower
  }

  def halfHull(points: List[Point]): List[Point] = {
    val upper = new ListBuffer[Point]()
    for (p <- points) {
      while (upper.size >= 2 && leftTurn(p, upper(0), upper(1))) {
        upper.remove(0)
      }
      upper.prepend(p)
    }
    upper.toList
  }

  def leftTurn(p1: Point, p2: Point, p3: Point): Boolean = {
    val slope = (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x)
    val collinear = math.abs(slope) <= 1e-9
    val leftTurn = slope < 0
    collinear || leftTurn
  }

  def perimeter(hull: List[Point]): Double = {
    def iter(points: List[Point], acc: Double): Double = points match {
      case p1 :: p2 :: rest => iter(p2 :: rest, acc + p1.distanceTo(p2))
      case p :: Nil         => acc + p.distanceTo(hull.head)
      case Nil              => acc
    }
    iter(hull, 0.0)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.tail)
  }

  def printResult(ls: List[String]): Unit = {
    val points = ls.map(s => { val a = s.split(" "); Point(a(0).toDouble, a(1).toDouble) })
    val hull = convexHull(points)
    println(perimeter(hull))
  }

}