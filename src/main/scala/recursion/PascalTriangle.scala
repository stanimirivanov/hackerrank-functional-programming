package recursion

/**
 * For a given integer K, print the first K rows of Pascal's Triangle.
 * Print each row with values separated by spaces.
 * The value at n-th row and r-th column of the triangle is equal to n! / (r! * (n-r)!)
 * where indexing start from 0. These values are the binomial coefficients.
 *
 * @see https://www.hackerrank.com/challenges/pascals-triangle
 */
object PascalTriangle {

  def triangle(row: Int): List[Int] = {
    row match {
      case 1 => List(1)
      case n: Int => List(1) ::: ((triangle(n - 1) zip triangle(n - 1).tail) map { case (a, b) => a + b}) ::: List(1)
    }
  }

  def printTriangle(n: Int): Unit = {
    (1 to n) foreach {
      i => triangle(i) map (c => print(c + " ")); println
    }
  }

  def main(args: Array[String]) {
    val k = io.Source.stdin.getLines().toList.head.toInt
    printTriangle(k)
  }

}
