package recursion

/**
 * Given a list of N integers A = [a1, a2, ..., aN], you have to find those integers
 * which are repeated at least K times. In case no such element exists you have to print -1.
 *
 * If there are multiple elements in A which are repeated at least K times, then
 * print these elements ordered by their first occurrence in the list.
 *
 * Let's say A = [4, 5, 2, 5, 4, 3, 1, 3, 4] and K = 2. Then the output is 4 5 3
 *
 * @see http://www.hackerrank.com/challenges/filter-elements
 */
object FilterElements {

  def filter(seq: List[String], k: Int): List[String] = {
    val numElements = seq.groupBy(s => s).map(t => (t._1, t._2.length))
    var filtered = List[String]()

    def numElementsPredicate(s: String): Boolean = numElements.get(s) match {
      case Some(n) => n >= k
      case None => false
    }

    seq.filter { s =>
      if (filtered.indexOf(s) == -1) {
        val p = numElementsPredicate(s)
        if (p) filtered = s :: filtered
        p
      } else {
        false
      }
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(ls => printResult(ls))
  }

  def printResult(ls: List[String]): Unit = {
    val k = ls.head.split(" ").toList.tail.head.toInt
    val seq = ls.tail.head.split(" ").toList
    val result = filter(seq, k)
    if (result.isEmpty) println(-1) else println(result.mkString (" "))
  }

}
