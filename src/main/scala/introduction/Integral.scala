package introduction

/**
 * Definite Integrals via Numerical Methods
 * This relates to definite integration via numerical methods.
 * Consider the algebraic expression given by:
 *
 * (a1)x^b1 + (a2)x^b2 + (a3)x^b3 ......(an)x^bn
 * For the purpose of numerical computation, the area under the
 * curve y = f(x) between the limits a and b can be computed by the
 * Limit Definition of a Definite Integral.
 *
 * Using equal Sub-Intervals of length = 0.001, you need to
 * 1) Evaluate the area bounded by a given polynomial function of
 *    the kind described above, between given limits a and b.
 * 2) Evaluate the volume of the solid obtained by revolving this
 *    polynomial curve around the X-Axis.
 *
 * Round off your answers to one decimal place(x.x).
 * An error margin of +/- 0.2 will be tolerated.
 *
 * @see http://www.hackerrank.com/challenges/area-under-curves-and-volume-of-revolving-a-curv
 */
object Integral extends App {

  // This function will be used while invoking "Summation" to compute
  // The area under the curve.
  def f(coefficients: List[Int], powers: List[Int],  x: Double): Double = {
    val cp = coefficients.view.zip(powers)
    cp.map { case (c, p) => c * math.pow(x, p) }.sum
  }

  // This function will be used while invoking "Summation" to compute
  // The Volume of revolution of the curve around the X-Axis
  // The 'Area' referred to here is the area of the circle obtained
  // By rotating the point on the curve (x,f(x)) around the X-Axis
  def area(coefficients: List[Int], powers: List[Int], x: Double): Double = {
    val a = f(coefficients, powers, x)
    math.Pi * math.pow(a, 2)
  }

  // This is the part where the series is summed up
  // This function is invoked once with func = f to compute the area
  // under the curve
  // Then it is invoked again with func = area to compute the volume
  // of revolution of the curve
  def summation(func: (List[Int], List[Int], Double) => Double,
                upperLimit: Int,
                lowerLimit: Int,
                coefficients: List[Int],
                powers:List[Int]): Double = {
    val range = (lowerLimit * 1000 to upperLimit * 1000)
    val terms = range.map(e => func(coefficients, powers, e * 0.001) * 0.001)
    math.floor(terms.sum * 10) / 10
  }

  // The Input-Output functions will be handled by us. You only need to
  // concentrate your effort on the function bodies above.

  def displayAnswers(coefficients: List[Int], powers: List[Int], limits: List[Int]) {
    println(summation(f, limits.reverse.head, limits.head, coefficients, powers))
    println(summation(area, limits.reverse.head, limits.head, coefficients, powers))
  }

  /** Purely IO Section **/
  val lines = io.Source.stdin.getLines.toArray
  val coefficients: List[Int] = lines(0).split(" ").toList.map(_.toInt)
  val powers: List[Int] = lines(1).split(" ").toList.map(_.toInt)
  val limits: List[Int] = lines(2).split(" ").toList.map(_.toInt)

  displayAnswers(coefficients, powers, limits)

}
