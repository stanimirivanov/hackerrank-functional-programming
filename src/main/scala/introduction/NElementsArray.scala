package introduction

/**
 * Create an array of N integers, where N is specified as input.
 * This can be any array with N integers.
 * For example, for N=4 you could return [1,1,1,1] or [1,2,3,4]
 *
 * @see http://www.hackerrank.com/challenges/fp-array-of-n-elements
 */
object NElementsArray extends App {

  def f(num: Int): List[Int] = List.fill(num)(1)

  val lines = io.Source.stdin.getLines.toList
  val num: Int = lines.head.toInt
  val result = f(num)
  println(result.length)

}
