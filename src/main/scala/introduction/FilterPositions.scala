package introduction

/**
 * For a given list with N integers, return a new list removing the elements
 * at odd indices, i.e. odd positions.
 *
 * @see http://www.hackerrank.com/challenges/fp-filter-positions-in-a-list
 */
object FilterPositions extends App {

  def f(arr: List[Int]): List[Int] =
    arr.view.zipWithIndex.filter(_._2 % 2 == 1).map(_._1).force.toList

  println(f(io.Source.stdin.getLines.toList.map(_.trim).map(_.toInt)).mkString("\n"))

}
