package introduction

/**
 * Update the values of a list with their absolute values.
 * The elements order must be preserved.
 *
 * @see http://www.hackerrank.com/challenges/fp-update-list
 */
object UpdateList extends App {

  def f(arr: List[Int]) : List[Int] = arr.map(x => math.abs(x))

  println(f(io.Source.stdin.getLines.toList.map(_.trim).map(_.toInt)).map(_.toString).mkString("\n"))

}
