package introduction

/**
 * Given a list repeat each element of the list n times.
 *
 * @see http://www.hackerrank.com/challenges/fp-list-replication
 */
object ListReplication extends App {

  def f(num: Int, arr: List[Int]): List[Int] =
    arr.foldLeft[List[Int]](List())((acc, n) => acc ++ List.fill(num)(n))

  def displayResult(arr: List[Int]) =
    println(f(arr(0), arr.drop(1)).map(_.toString).mkString("\n"))

  displayResult(io.Source.stdin.getLines.toList.map(_.trim).map(_.toInt))

}
