package introduction

/**
 * The series expansion of e^x is given by:
 *
 * 1 + x + x2/2! + x3/3! + x4/4! + .......
 *
 * Evaluate e^x for given values of x, by using the above expansion for the first 10 terms.
 *
 * The result should be correct up to 4 decimal places.
 *
 * @see http://www.hackerrank.com/challenges/eval-ex
 */
object Exp extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  lazy val factorials: Stream[BigInt] = BigInt(1) #:: factorials.zip(N).map(a => a._1 * a._2)

  def factorial(n: Int): BigInt = factorials.take(n + 1).last

  def f(x: Double): Double = {
    (0 to 100).map(n => math.pow(x, n) / factorial(n).toDouble).sum
  }

  val lines = io.Source.stdin.getLines.toList.map(_.toDouble).tail
  lines.foreach(x => println(f(x)))

}
