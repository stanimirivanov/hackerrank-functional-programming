package introduction

/**
 * Count the number of elements in an array without using
 * count, size or length operators (or their equivalents).
 *
 * @see http://www.hackerrank.com/challenges/fp-list-length
 */
object ListLength extends App {

  def f(arr: List[Int]): Int = arr.foldLeft(0)((acc, _) => acc + 1)
  
  println(f(io.Source.stdin.getLines.toList.map(_.trim).map(_.toInt)))

}
