package introduction

/**
 * Filter a given array of integers and leave only that values which are
 * less than a specified value X. The integers in the output should be in
 * the same sequence as they were in the input.
 *
 * You have to write your own implementation of filter function.
 * Use of inbuilt library function is deprecated.
 *
 * @see http://www.hackerrank.com/challenges/fp-filter-array
 */
object FilterArray extends App {

  def f(delim: Int, arr: List[Int]): List[Int] =
    for {
      n <- arr
      if (n < delim)
    } yield n

  var lines = io.Source.stdin.getLines.toList
  println(f(lines(0).toInt, lines.map(_.trim).map(_.toInt)).map(_.toString).mkString("\n"))

}
