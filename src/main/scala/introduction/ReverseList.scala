package introduction

/**
 * Reverse a list without using reverse function.
 *
 * @see http://www.hackerrank.com/challenges/fp-reverse-a-list
 */
object ReverseList extends App {

  def f(arr:List[Int]):List[Int] = arr.foldLeft[List[Int]](List())((acc, x) => x :: acc)

  println(f(io.Source.stdin.getLines.toList.map(_.trim).map(_.toInt)).mkString("\n"))

}
