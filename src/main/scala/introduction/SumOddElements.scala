package introduction

/**
 * Return sum of odd elements from an list.
 *
 * @see http://www.hackerrank.com/challenges/fp-sum-of-odd-elements
 */
object SumOddElements extends App {

  def f(arr: List[Int]): Int = arr.filter(n => math.abs(n % 2) == 1).sum
  
  println(f(io.Source.stdin.getLines.toList.map(_.trim).map(_.toInt)))

}
