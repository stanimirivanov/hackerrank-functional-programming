package memo

/**
 * A binary tree is a tree which is characterized by any of the following
 * properties:
 *
 * 1. It can be empty (null).
 * 2. It can contain a root node which contain some value and two subtree,
 *    left subtree and right subtree, which are also binary tree.
 *
 * A binary tree is a binary search tree (BST) if all the non-empty nodes
 * follows both two properties:
 *
 * 1. If node has a left subtree, then all the values in its
 *    left subtree are smaller than the value of the current node.
 * 2. If node has a right subtree, then all the value in its
 *    right subtree are greater than the value of the current node.
 *
 * You are given N nodes, each having unique value ranging from [1, N],
 * how many different binary search tree can be created using all of them.
 *
 * @see http://www.hackerrank.com/challenges/number-of-binary-search-tree
 */
object BinarySearchTreeCount {

  val memo = scala.collection.mutable.HashMap.empty[Int, BigInt]

  def countBST(n: Int): BigInt = {
    (1 to n).foldLeft(BigInt(0)) { (acc, k) =>
      val left = memo.getOrElseUpdate(k - 1, countBST(k - 1))
      val right = memo.getOrElseUpdate(n - k, countBST(n - k))
      acc + (left * right)
    }
  }

  def init() {
    memo += (0 -> BigInt(1))
    memo += (1 -> BigInt(1))
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    init()
    val m = BigInt(10).pow(8) + 7
    val result = countBST(s.toInt)
    println(result % m)
  }

}
