package memo

/**
 * This series can be broken down as the following series:
 *
 * Fib0 = 0
 * Fib1 = 1
 * Fibn = Fibn-1 + Fibn-2 , n > 1
 *
 * First few elements of Fibonacci series are: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377...
 *
 * You are given a list of non-negative integers. For each integer, n,
 * in the list print nth fibonacci number modulo 108+7.
 *
 * @see http://www.hackerrank.com/challenges/fibonacci-fp
 */
object Fibonacci {

  lazy val fib: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fib.zip(fib.tail).map(p => p._1 + p._2)

  def fibonacci(n: Int): BigInt = fib.take(n + 1).last

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().drop(1).toList
    lines.foreach {
      line => println(fibonacci(line.toInt) % (BigInt(10).pow(8) + BigInt(7)))
    }
  }

}
