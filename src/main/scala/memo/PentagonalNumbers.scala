package memo

/**
 * Pentagonal numbers are the number of dots that can be shown in a pentagonal
 * pattern of dots. Let's represent the nth pentagonal number by P(n). The
 * following figure depicts pentagonal patterns for n in {1, 2, 3, 4, 5}.
 *
 * [IMG]
 *
 * Your task is to find the value of P(n) for a given n.
 *
 * @see https://www.hackerrank.com/challenges/pentagonal-numbers
 */
object PentagonalNumbers {

  def pentagonal(n: Long): BigInt = n * (3 * n - 1) / 2

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(s => printResult(s))
  }

  def printResult(s: String): Unit = {
    val n = s.toLong
    val result = pentagonal(n)
    println(result)
  }

}
